<?php
/*
Plugin Name: MyCPTPlugin
Plugin URI: SANS URI
Description: Plugin CPTUI Apprenants (Taxonomies : Promotion & Competence)
Version: 1.0.0
Author: Thomas Po
Author URI: SANS URI
Text Domain:
Domain Path: /lang
*/

add_theme_support('post-thumbnails', array('post', 'apprenants'));
require_once('fonctions/cpt.php');
require_once('fonctions/acf.php');

