<?php



// fonction ajout CPT (custom post type) Apprenant
function cptui_register_my_cpts()
{

  /**
   * Post Type: Apprenants.
   */

  $labels = [
    "name" => __("Apprenants", "twentynineteen"),
    "singular_name" => __("Apprenant", "twentynineteen"),
  ];

  $args = [
    "label" => __("Apprenants", "twentynineteen"),
    "labels" => $labels,
    "description" => "Apprenants de la formation dev web et web mobile",
    "public" => true,
    "publicly_queryable" => true,
    "show_ui" => true,
    "show_in_rest" => true,
    "rest_base" => "",
    "rest_controller_class" => "WP_REST_Posts_Controller",
    "has_archive" => false,
    "show_in_menu" => true,
    "show_in_nav_menus" => true,
    "delete_with_user" => false,
    "exclude_from_search" => false,
    "capability_type" => "post",
    "map_meta_cap" => true,
    "hierarchical" => false,
    "rewrite" => ["slug" => "apprenants", "with_front" => true],
    "query_var" => true,
    "supports" => ["title", "thumbnail"],
    "show_in_graphql" => false,
  ];

  register_post_type("apprenants", $args);
}

add_action('init', 'cptui_register_my_cpts');


//création fonction ajout de la taxonomy Promotion
function cptui_register_my_taxes() {

	/**
	 * Taxonomy: Promos.
	 */

	$labels = [
		"name" => __( "Promos", "twentynineteen" ),
		"singular_name" => __( "Promos", "twentynineteen" ),
	];

	
	$args = [
		"label" => __( "Promos", "twentynineteen" ),
		"labels" => $labels,
		"public" => true,
		"publicly_queryable" => true,
		"hierarchical" => true,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => [ 'slug' => 'promo_2', 'with_front' => true, ],
		"show_admin_column" => false,
		"show_in_rest" => true,
		"show_tagcloud" => false,
		"rest_base" => "promo_2",
		"rest_controller_class" => "WP_REST_Terms_Controller",
		"show_in_quick_edit" => false,
		"show_in_graphql" => false,
	];
	register_taxonomy( "promo_2", [ "apprenants" ], $args );

	/**
	 * Taxonomy: Compétences.
	 */

	$labels = [
		"name" => __( "Compétences", "twentynineteen" ),
		"singular_name" => __( "Compétence", "twentynineteen" ),
	];

	
	$args = [
		"label" => __( "Compétences", "twentynineteen" ),
		"labels" => $labels,
		"public" => true,
		"publicly_queryable" => true,
		"hierarchical" => false,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => [ 'slug' => 'competences', 'with_front' => true, ],
		"show_admin_column" => false,
		"show_in_rest" => true,
		"show_tagcloud" => false,
		"rest_base" => "competences",
		"rest_controller_class" => "WP_REST_Terms_Controller",
		"show_in_quick_edit" => false,
		"show_in_graphql" => false,
	];
	register_taxonomy( "competences", [ "apprenants" ], $args );
}
add_action( 'init', 'cptui_register_my_taxes' );



